# TTree Tool

Tool to do some command line operations on root `TTree`s. 

```
>make
>./ttree-tool help
```

```
OPTION: help
DESCRIPTION: General usage: ./ttree-tool <option> <option values> 
Besides "help" the options are:

OPTION: info
DESCRIPTION: prints general info about the file:
/ttree-tool info file.root

OPTION: cut
DESCRIPTION: provide the cut string expression for the TTree:
./ttree-tool cut "A > 3 && B*B == 2 " tree out.root in1.root in2.root

OPTION: select-branches
DESCRIPTION: provide the branch names to keep in the output file separated by a ",":
./ttree-tool select-brances "A, B" tree out.root in1.root in2.root

OPTION: deselect-branches
DESCRIPTION: provide the branch names to keep in the output file separated by a ",":
./ttree-tool deselect-brances "A, B" tree out.root in1.root in2.root
```
