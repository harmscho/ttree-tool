CXX=g++ -std=c++11
ROOT_INC = $(shell root-config --cflags)
ROOT_LIB = $(shell root-config --libs)

all: ttree-tool.cc
	$(CXX) ttree-tool.cc -o ttree-tool $(ROOT_INC) $(ROOT_LIB)

