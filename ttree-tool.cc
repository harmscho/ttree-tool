//C++
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
//ROOT
#include "TROOT.h"
#include "TEventList.h"
#include "TTree.h"
#include "TChain.h"
#include "TString.h"
#include "TFile.h"
//some global variables
TChain* gChain;
TFile* gFileOut;
TTree* gTreeOut;

using namespace std;

struct Option {
  string name;
  string descr;
};
vector<Option> options;

void InitOptions(){
  Option o;
  o.name = "help";
  o.descr = "General usage: ./ttree-tool <option> <option values> \nBesides \"help\" the options are:";
  options.push_back(o);
  
  o.name = "info";
  o.descr = "prints general info about the file:\n/ttree-tool info file.root";
  options.push_back(o);
  
  o.name = "cut";
  o.descr = "provide the cut string expression for the TTree:\n./ttree-tool cut \"A > 3 && B*B == 2 \" tree out.root in1.root in2.root";
  options.push_back(o);

  o.name = "select-branches";
  o.descr = "provide the branch names to keep in the output file separated by a \",\":\n./ttree-tool select-branches \"A, B\" mytree out.root in1.root in2.root";
  options.push_back(o);

  o.name = "deselect-branches";
  o.descr = "provide the branch names to keep in the output file separated by a \",\":\n./ttree-tool deselect-branches \"A, B\" mytree out.root in1.root in2.root";
  options.push_back(o);  
}

void PrintOption(string optName) {
  for(Option& o:options) {
    if (o.name == optName) {
      cout << o.name << ": " <<  o.descr << endl;
    }
  }
}

void Cut(int argc,char** argv){    
  if (argc < 6) {
      cout << "Not enough arguments specified." << endl;
      PrintOption("cut");
      exit(-1);
  }
  
  gChain = new TChain(argv[3],argv[3]);
  for (int i = 5; i < argc; i++) {  
    gChain->Add(argv[i]);      
  }
  
  TString cut = argv[2];
  gChain->Draw(">>sel",cut,"goff");
  TEventList *elist = (TEventList*)gDirectory->Get("sel");
  cout << "Entries before selection "<< gChain->GetEntries() << endl;
  gChain->SetEventList(elist);
  gFileOut = new TFile(argv[4],"recreate");
  gTreeOut = gChain->CopyTree("");
  cout << "Entries After selection "<< gTreeOut->GetEntries() << endl;
  gTreeOut->Write();
  gFileOut->Close();    
}

void PrintOption(Option& o){
    cout << "OPTION: " << o.name << endl;
    cout << "DESCRIPTION: " << o.descr  << endl;
    cout << "" << endl;
    // cout << << endl;
}

void Info(int argc,char** argv) {
  cout << "File info" << endl;
  cout << "---------" << endl;
  //only use one file
  if (argc != 3) {
    cout << "Incorrect number of arguments" <<endl;
    PrintOption("info");
    exit(-1);
  }
  gFileOut = new TFile(argv[2]);
  gFileOut->Print();
  TList* l = gFileOut->GetListOfKeys();
  int n = l->GetSize();
  cout << "number of keys in file: " << n << endl;
  for (int i = 0; i < n; i++) {
    string className = gFileOut->Get(l->At(i)->GetName())->ClassName();
    cout <<  className << " " << l->At(i)->GetName();
    if (className == "TTree") {
      cout << ", branches: \"";
      TTree* t =  (TTree*) gFileOut->Get(l->At(i)->GetName());
      auto objArr = t->GetListOfBranches();
      for (int ib = 0; ib < objArr->GetEntries(); ib++) {
        cout << objArr->At(ib)->GetName();
        if (ib < objArr->GetEntries() -1) { 
          cout << ", ";
        } else {
          cout << "\"";
        }
      }
      cout << endl;
      
    }    
  }
}

void SelectBranches(int argc,char** argv, bool selectBranches = true) {
  if (argc < 6) {
      cout << "Not enough arguments specified." << endl;
      if (selectBranches) {
        PrintOption("select-branches");
      } else PrintOption("deselect-branches");
      exit(-1);
  }
  
  gChain = new TChain(argv[3],argv[3]);
  for (int i = 5; i < argc; i++) {  
    gChain->Add(argv[i]);      
  }
  
  stringstream ss(argv[2]);
  vector<string> branchNames;
  string str;
  while(getline( ss, str, ',' ))
  {
    //some magic to remove white spaces
    str.erase(remove_if(str.begin(), str.end(),::isspace), str.end());
    branchNames.push_back( str );    
  }
  
  if (selectBranches) {
    gChain->SetBranchStatus("*",0);
  } else {
    gChain->SetBranchStatus("*",1);
  }

  for (string& bn:branchNames){
    if (selectBranches) {
      gChain->SetBranchStatus(bn.c_str(),1);
    } else {
      gChain->SetBranchStatus(bn.c_str(),0);
    }
    
  }  
  gFileOut = new TFile(argv[4],"recreate");
  gTreeOut = gChain->CopyTree("");
  cout << "Entries After selection "<< gTreeOut->GetEntries() << endl;
  gTreeOut->Write();
  gFileOut->Close();
}

void DeSelectBranches(int argc,char** argv) {
    SelectBranches(argc,argv,false);
}

void Help() {
  for(Option& o:options) {
    PrintOption(o);
  }
}

int main(int argc,char** argv) {  
  
  InitOptions();
  if (argc < 2) {
    cout << "--------------------" << endl;
    cout << "No option specified!" << endl;
    cout << "--------------------\n" << endl;
    Help();
    return -1;
  }
  
  string opt = argv[1];    
  if(opt == "cut") {
    Cut(argc,argv); 
  } else if (opt == "select-branches") {
    SelectBranches(argc,argv);
  } else if (opt == "deselect-branches") {
    DeSelectBranches(argc,argv);  
  } else if (opt == "info") {  
    Info(argc,argv);
  } else {
    if (opt != "help") {
      cout << "Unkown option!" << endl;
    } 
    Help();
  }
    
  return 0;
}
